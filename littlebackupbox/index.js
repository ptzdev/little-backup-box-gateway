var passport = require('passport');
var restify = require('restify');
var Strategy = require('passport-http').BasicStrategy;

exports.users = users = require('./users');
exports.core = core = require('./lbb');
exports.initialize = function(passport) {
    passport.use(new Strategy(
        function(username, password, cb) {
            users.findByUsername(username, function(err, user) {
            if (err) { return cb(err); }
            if (!user) { return cb(null, false); }
            if (user.password != password) { return cb(null, false); }
            return cb(null, user);
            });
    }));
}

exports.setEndpoints = function (server){
    setEndpointWithAuthGet(server,  '/api/list', core.listFiles);
    setEndpointWithAuthPost(server, '/api/emptyDrive', core.emptyDrive);
    setEndpointWithAuthPost(server, '/api/turnoff', core.shutDown);
    setEndpointWithAuthPost(server, '/api/reboot', core.reboot);
    server.get('/api/getFile/:id', core.getFile);
    server.get(/.*/, restify.serveStatic({
        'directory': '/lbb-g/static_content',
        'default': 'index.html'
     }));
}

function setEndpointWithAuthGet(server, endpoint, action){
    server.get(endpoint, passport.authenticate('basic', { session: false }), action);
}

function setEndpointWithAuthPost(server, endpoint, action){
    server.post(endpoint, passport.authenticate('basic', { session: false }), action);
}