var recursive = require('recursive-readdir');
var rimraf = require('rimraf');
var fs = require('fs');
var Jimp = require("jimp");
var path = require('path');
const storageFolder = '/media/storage/';
//const storageFolder = './photos';

function LittleBackUpBoxController () {
    var that = this;

    that.listFiles = function (req,res, next){
        recursive(storageFolder, ["CARD_ID","*.NEF","*.DSC","IndexerVolumeGuid","*.dat","*_thumb.JPG"],function (err, files) {
            var slash = require('slash');
            var util = require('util')
            var returnJSON = [];

            for (var i = 0, len = files.length; i < len; i++) {
                var filename = slash(files[i]);
                var stats = fs.statSync(filename);
                var mtime = new Date(util.inspect(stats.mtime));
                path = filename.replaceAll(".JPG","_thumb.JPG");
                var imageFile ="{url}/api/getFile/"+path.replaceAll("/","+");

                returnJSON.push({
                    Filename : filename,
                    Image : imageFile,
                    Date : mtime
                });
            }

            res.send(returnJSON);

            return next();
        });
    };

    that.getFile = function (req,res, next){
        var id = req.params.id;
        var filename = '/'+id.replaceAll("+","/");
        var img = fs.readFileSync(filename);
        res.writeHead(200, {'Content-Type': 'image/jpg' });
        res.end(img, 'binary');
        return next();
    };
    
    that.emptyDrive = function (req,res, next){
        var files = fs.readdirSync(storageFolder);

        for (var i in files) {
            rimraf(storageFolder+"/"+files[i],  function () {
                return next();
            });
            
        }

        files = fs.readdirSync("./thumb/");

        for (var i in files) {
            rimraf("./thumb/"+files[i],  function () {
                return next();
            });
            
        }
        res.send('The request was sent');
        return next();
    };

    that.shutDown = function (req,res, next){
        var powerOff = require('power-off');

        powerOff( function (err, stderr, stdout) {
            if(!err && !stderr) {
                console.log(stdout);
            }
        });

        res.send('The Little Backupbox will soon Shut Down');

        return next();
    };

    that.reboot = function (req,res, next){
        var reboot = require('nodejs-system-reboot');

        reboot( function (err, stderr, stdout) {
            if(!err && !stderr) {
                console.log(stdout);
            }
        });

        res.send('The Little Backupbox will soon reboot');

        return next();
    };
};

module.exports = new LittleBackUpBoxController();

String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.split(search).join(replacement);
};