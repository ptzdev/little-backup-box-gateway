var restify = require('restify'),
    passport = require('passport'),
    lbb = require('./littlebackupbox'),
    port = process.env.port || 3000;

var serverA = restify.createServer();
var serverB = restify.createServer();

serverA.use(function(req,res,next){
    console.log(req.method + ' '+ req.url);
    return next();
});
serverA.use(restify.bodyParser());
serverA.use(passport.initialize());
serverB.use(function(req,res,next){
    console.log(req.method + ' '+ req.url);
    return next();
});
serverB.use(restify.bodyParser());
serverB.use(passport.initialize());

lbb.initialize(passport);
lbb.setEndpoints(serverA);
lbb.setEndpoints(serverB);

serverA.listen(port, function() {
  console.log('%s listening at %s', serverA.name, serverA.url);
});
serverB.listen(80, function() {
  console.log('%s listening at %s', serverB.name, serverB.url);
});
